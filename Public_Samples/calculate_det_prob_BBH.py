#!/usr/bin/env python
# coding: utf-8

# In[1]:


from __future__ import division, print_function
import bilby
import numpy as np
import h5py
from tqdm import tqdm
from scipy.stats import ncx2
import sys

bilby.core.utils.setup_logger(log_level='warning')


# In[ ]:





# In[2]:


def calculate_det_prob_matrix(N_inj,duration=4,t_inj=0,sampling_frequency=2048,ifos_list=['H1','L1','V1'],psd_dict=None,
                         approx=['IMRPhenomPv3HM'],d_array=np.linspace(100,10000,50),
                         priors_dict=bilby.gw.prior.BBHPriorDict(), 
                         sample_params=['a_1','a_2','tilt_1','tilt_2','phi_12','phi_jl','theta_jn',
                                        'psi','phase','ra','dec','mass_1','mass_2']):
    
    injections_dict=priors_dict.sample_subset(keys=sample_params,size=N_inj)
    injections_dict['geocent_time']=np.random.rand(N_inj)*3.14e7 + 1126259642.413
        
    det_prob = np.zeros_like(d_array)
    
    
    j=0
    for d_l in tqdm(d_array):
        
        snr_array = []
        
        for i in range(N_inj):
            opt_SNR = 0
            for wave in approx:
                injection_parameters = {field:injections_dict[field][i] for field in injections_dict.keys()}
                injection_parameters['luminosity_distance'] = d_l
                injection_parameters['geocent_time'] = injections_dict['geocent_time'][i]
        
                waveform_arguments = dict(waveform_approximant=wave,
                          reference_frequency=50.)

                waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
                    sampling_frequency=sampling_frequency, duration=duration,
                    frequency_domain_source_model=bilby.gw.source.lal_binary_black_hole,
                    parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,
                    waveform_arguments=waveform_arguments)
            
                ifos = bilby.gw.detector.InterferometerList(ifos_list)
                for i in range(len(ifos)):
                    ifos[i].power_spectral_density=psd_dict[ifos_list[i]]['C01:'+wave]
                ifos.set_strain_data_from_power_spectral_densities(
                    sampling_frequency=sampling_frequency, duration=duration,
                    start_time=injection_parameters['geocent_time'] - t_inj)
                try:
                    ifos.inject_signal(waveform_generator=waveform_generator,
                               parameters=injection_parameters)
                except:
                    print(injection_parameters)
                    sys.exit()
            
                SNR = 0
                for ifo_string in ifos_list:
                    SNR+=np.abs(ifos.meta_data[ifo_string]['optimal_SNR'])**2
                opt_SNR+=SNR
            snr_array.append(opt_SNR/len(approx))
            
        snr_array=np.array(snr_array)
        det_prob[j]=np.sum(ncx2.sf(12*12,df=2*len(ifos_list),nc=snr_array))/N_inj
        j+=1
        
    return det_prob, d_array

            
    
    


# In[3]:


priors_bh=riors_dict=bilby.gw.prior.BBHPriorDict()
priors_bh['mass_1']=bilby.prior.Uniform(minimum=30,maximum=200)
priors_bh['mass_2']=bilby.prior.Uniform(minimum=30,maximum=200)


# In[4]:


import h5py

ifos_list = ['H1','L1','V1']
approx = ['C01:IMRPhenomPv3HM','C01:NRSur7dq4', 'C01:SEOBNRv4PHM']
f = h5py.File('GW190521A.h5', 'r')
waveforms = ['IMRPhenomPv3HM','NRSur7dq4', 'SEOBNRv4PHM']
psds_dict = {'H1':{},'L1':{},'V1':{}}
for ifo in ifos_list:
    for wave in approx:
        psds_dict[ifo][wave]=bilby.gw.detector.PowerSpectralDensity(frequency_array=f[wave]['psds'][ifo][:,0],
                                      psd_array=f[wave]['psds'][ifo][:,1])


# In[5]:


det_prob, d_array=calculate_det_prob_matrix(1000,duration=4,t_inj=0,sampling_frequency=1024,ifos_list=['H1','L1','V1'],psd_dict=psds_dict,
                         approx=waveforms,d_array=np.linspace(100,10000,200),
                         priors_dict=priors_bh)


# In[8]:


data=np.column_stack([d_array,det_prob])
np.savetxt('det_prob_BBH.dat',data)


# In[ ]:




