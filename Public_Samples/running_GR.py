#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
from astropy.cosmology import FlatLambdaCDM
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
from scipy.interpolate import interp1d
from scipy.optimize import fsolve
import bilby
import h5py
from scipy.integrate import quad


# In[2]:


from astropy.cosmology import Planck15


# The Luminosity distance for this model is
# 
# $$d_L^{\rm GW} = d_L^{\rm LCDM} exp \left[\frac{c_M}{2\Omega_{\Lambda,0}} \ln \frac{1+z}{\Omega_{m,0}(1+z)^3+\Omega_{\Lambda,0}} \right]$$
# 
# Taken from https://arxiv.org/pdf/1901.03321.pdf
# 
# GR is recovered for $c_m=0$

# ## Statistical method
# 
# Below we write the likelihood for the EM data and GW data conditioned on the cosmological parameters $\vec{\Lambda}=[H_0,\Omega_m]$ and a set of testing GR parameters $\vec{\alpha}$, see Eq. above. We use flat LCDM cosmology with running $H_0$ and $\Omega_m$. If we assume uniform priors on masses in the detector frame
# 
# $$p(x_{\rm GW},x_{\rm EM}|\vec{\Lambda},\vec{\alpha})= \int p(x_{\rm GW}|d_l(z,\vec{\Lambda},\vec{\alpha})) p(x_{\rm EM}|z) p(z|\Lambda) dz$$
# 
# And the selection effect can be computed as (assuming that you are always able to detect the AGN flare)
# 
# $$\beta(\vec{\Lambda},\vec{\alpha})= \int_0^{z_{max}} p^{\rm GW}_{\rm det}(d_l(z,\vec{\Lambda},\vec{\alpha})) p(z|\Lambda) dz $$
# 
# The motivation for using uniform prior on masses are following:
# 
# * We need to find the line-of-sight posterior from the posterior samples. When doing so, we implicitely marginalize on masses (no 5 dimensional fit). Assuming uniform masses in the detector frame makes everything easier from computation point of view.
# 
# * Also the selection effect should be evaluated in terms p_{det}(m_1,m_2,d_l). Knowing this function would require some 3D fit or Montecarlos for the selection function, i.e. for every MCMC step you draw N binaries from priors and then compute the detected fraction...very heavy
# 
# * We want something quick and dirty at this step

# In[ ]:





# In[3]:


def gw_luminosity_distance(z,cosmology,cm):
    """ This function computes the Gw luminosity distance
    
    Parameters
    ----------
    z: array
        Cosmological redshift
    cosmology: astropy.cosmo
        cosmology class from astropy
    cm: float
        Friction parameter
    
    Returns
    -------
    Luminosity distance in Mpc
    
    """
    
    dl_cosmo = cosmology.luminosity_distance(z).value
    Om0 = cosmology.Om0
    OL0 = 1-Om0
    
    return dl_cosmo*np.exp((0.5*cm/OL0)*np.log((1+z)/(OL0+Om0*(1+z)**3)**(1/3)))


# In[4]:


def selection_effect(d_max,z_guess,pdet,cosmology,cm):
    """ Calculates the selection effect
    
    Parameters
    ----------
    z_array: array
        array in redshift used to compute the selection effect
    pdet: interpolation
        interpolation of the GW detection probability in Mpc
    cosmology: astropy.cosmo
        cosmology class from astropy
    cm: float
        Friction parameter
    
    Returns
    -------
    The integral of the selection effect
    
    """
    
    funct_to_solve = lambda z: gw_luminosity_distance(z,cosmology,cm)-d_max
    
    zcrit=0 
    while zcrit==0:
        zcrit=fsolve(funct_to_solve,z_guess, xtol=0.001)[0] # Initial solution of z_guess
    
    z_array=np.logspace(-6,np.log10(zcrit),1000)
    
    dl_gw = gw_luminosity_distance(z_array,cosmology,cm)
    prior = cosmology.differential_comoving_volume(z_array).value/(1+z_array)


    return np.trapz(pdet(dl_gw)*prior,z_array)


# In[5]:


class modGR_likelihood(bilby.Likelihood):

    def __init__(self,gkde_posterior,z_samples,pdet,d_max):

        super().__init__(parameters={})
        self.gkde_posterior=gkde_posterior
        self.pdet=pdet
        self.d_max=d_max
        self.z_samples=z_samples
        

    def log_likelihood(self):
        """ Returns the log likelihood
        """
                
        
        # Cosmo parameters
        H0 = self.parameters['H0']
        Om0 = self.parameters['Om0']
        
        # Deviation parameters
        cm = self.parameters['cm']
        
        cosmo = FlatLambdaCDM(H0=H0, Om0=Om0, Tcmb0=2.725)
        
        N_events = len(self.gkde_posterior)
        
        numerator=1
        sel_effect=1
        
        for i in range(N_events):
        
            dgws = gw_luminosity_distance(self.z_samples[i],cosmo,cm)

            sel_effect *= selection_effect(self.d_max[i],np.mean(self.z_samples[i]),self.pdet[i],cosmo,cm)
            
            evals=self.gkde_posterior[i](dgws)
            evals[evals<=1e-6]=1e-100
            
            if sel_effect==0: 
                print(self.parameters)
            numerator *= np.sum(evals*cosmo.differential_comoving_volume(
                self.z_samples[i]).value/((dgws**2)*(1+self.z_samples[i])))

        return np.log(numerator)-np.log(sel_effect)
        
        
        


# ##  analysis

# In[6]:


#provide here the list of events you want to analyse 
events = ['GW170817','GW190521']
mode = 'all'
wave = 'IMRPhenom'

for mode in ['all']:
    

        outdir = './running_GR_'+mode+'_GW170817_GW190521'

        pos_inter_likeli = []
        # Below load and fit with an interp1d the posterior of luminosity distance provided by LVC. 
        # Note: please, do not remove the original d**2 prior. It is taken into account in the likelihood

        for event in events:

            if event == 'GW170817':
                data = np.genfromtxt('../high_spin_PhenomPNRT_posterior_samples.dat',names=True)
                lum_distance_samples = data['luminosity_distance_Mpc']
                plt.figure()

                gkde_pos = gaussian_kde(lum_distance_samples)
                dprox = np.linspace(np.min(lum_distance_samples),np.max(lum_distance_samples),1000)
                interp_pos = interp1d(dprox,gkde_pos(dprox),bounds_error=False,fill_value=0)

                pos_inter_likeli.append(interp_pos)

                _ = plt.hist(gkde_pos.resample(1000)[0],bins='auto',label='gkde samples',density=True)
                _ = plt.hist(lum_distance_samples,bins='auto',label='actual samples',color='orange',density=True)
                plt.plot(dprox,interp_pos(dprox),label='interpolation',color='green')
                plt.title(event)

            elif event == 'GW190521':
                plt.figure()
                data = np.genfromtxt('LOS_fit_Public_Samples_1e-3deg2.txt')
                interp_pos = interp1d(data[:,0],data[:,1],bounds_error=False,fill_value=0)
                pos_inter_likeli.append(interp_pos)
                plt.plot(data[:,0],interp_pos(data[:,0]),label='interpolation',color='green')
                plt.plot(data[:,0],data[:,1],label='data',color='green')
                plt.title(event)


            plt.legend()
            plt.xlabel(r'$d_L$ [Mpc]')


        # In[7]:


        # Here it loads and and interpolate the precomputed detection probabilities

        det_interp_likeli = []
        d_max_likeli = []

        for event in events:

            if event == 'GW170817':
                data = np.genfromtxt('det_prob_BNS.dat')
                d_max_likeli.append(200)
            elif event == 'GW190521':
                data = np.genfromtxt('det_prob_BBH.dat')
                d_max_likeli.append(1e4)

            plt.figure()
            plt.title(event)

            plt.plot(data[:,0],data[:,1],label='numeric')
            interpolation_pdet = interp1d(data[:,0],data[:,1],bounds_error=False,fill_value=0)

            det_interp_likeli.append(interpolation_pdet)

            plt.plot(data[:,0],interpolation_pdet(data[:,0]),label='interpolation')
            plt.legend()
            plt.xlabel(r'$d_L$ [Mpc]')


        # In[8]:


        d_max_likeli


        # In[28]:


        # Here it setup the redshift uncertainties. For GW190521 we assume it known. For GW170817 we assume the 
        # distribution corrected for peculiar motion

        z_em_likeli = []

        for event in events:

            if event == 'GW170817':
                c=299792.458

                mu = np.array([3327/c, +310.0/c])
                sig = np.array([72/c,150/c])

                mu_tot= mu[0]-mu[1]
                sig_tot=np.sqrt(np.sum(sig**2))

                z_em_likeli.append(0.010033618884026258+0.0005445973324705001*np.random.randn(500))

            elif event == 'GW190521':
                z_em_likeli.append(0.438)

        likeli = modGR_likelihood(pos_inter_likeli,z_em_likeli,det_interp_likeli,d_max_likeli)



        # Set up priors, see bilby documentation




        priors = {}

        if mode == 'planck':
            priors['H0']=bilby.core.prior.Gaussian(mu=67.66,sigma=0.42,name='H0',latex_label='$H_0$')
            priors['Om0']=bilby.core.prior.Gaussian(mu=0.3111,sigma=0.0056,name='Om0',latex_label='$\Omega_{m,0}$')

        elif mode == 'all':
            priors['H0']=bilby.core.prior.Uniform(minimum=20,maximum=300,name='H0',latex_label='$H_0$')
            priors['Om0']=bilby.core.prior.Uniform(minimum=0.2,maximum=1.0,name='Om0',latex_label='$\Omega_{m,0}$')

        priors['cm']=0


        # In[29]:


        # Run the  samples, note change "outdir" if you want to create a new folder with the analysis.
        result=bilby.run_sampler(likelihood=likeli, priors=priors, outdir=outdir, 
                                 sampler='emcee',nwalkers=10,iterations=4000)


        # In[30]:


        result.plot_corner()


        # In[ ]:


        gw_luminosity_distance(0.01,FlatLambdaCDM(H0=150,Om0=0.31),120)


        # In[26]:


        z_array = np.linspace(0,0.6,1000)


        # In[27]:


        plt.plot(z_array)


        # In[ ]:




