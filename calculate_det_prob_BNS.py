#!/usr/bin/env python
# coding: utf-8

# In[2]:


from __future__ import division, print_function
import bilby
import numpy as np
import h5py
from tqdm import tqdm
from scipy.stats import ncx2
import sys

bilby.core.utils.setup_logger(log_level='warning')


# In[3]:


bilby.gw.prior.BNSPriorDict().keys()


# In[4]:


def calculate_det_prob_matrix(N_inj,duration=60,t_inj=0,sampling_frequency=2048,ifos_list=['H1','L1','V1'],psd_dict=None,
                         approx='IMRPhenomPv2_NRTidal',d_array=np.linspace(10,200,50),
                         priors_dict=bilby.gw.prior.BNSPriorDict()):
    
    injections_dict=priors_dict.sample(size=N_inj)
    injections_dict['geocent_time']=np.random.rand(N_inj)*3.14e7 + 1126259642.413
        
    det_prob = np.zeros_like(d_array)
    
    
    j=0
    for d_l in tqdm(d_array):
        
        snr_array = []
        
        for i in range(N_inj):
            
            injection_parameters = {field:injections_dict[field][i] for field in injections_dict.keys()}
            injection_parameters['luminosity_distance'] = d_l
            injection_parameters['geocent_time'] = injections_dict['geocent_time'][i]
        
            waveform_arguments = dict(waveform_approximant=approx,
                          reference_frequency=50.)

            waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
                sampling_frequency=sampling_frequency, duration=duration,
                frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
                parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
                waveform_arguments=waveform_arguments)
            
            ifos = bilby.gw.detector.InterferometerList(ifos_list)
            for i in range(len(ifos)):
                ifos[i].power_spectral_density=psd_dict[ifos_list[i]]
            ifos.set_strain_data_from_power_spectral_densities(
                sampling_frequency=sampling_frequency, duration=duration,
                start_time=injection_parameters['geocent_time'] - t_inj)
            try:
                ifos.inject_signal(waveform_generator=waveform_generator,
                               parameters=injection_parameters)
            except:
                print(injection_parameters)
                sys.exit()
            
            opt_SNR = 0
            for ifo_string in ifos_list:
                opt_SNR+=np.abs(ifos.meta_data[ifo_string]['optimal_SNR'])**2
            
            snr_array.append(opt_SNR)
            
        snr_array=np.array(snr_array)
        print(d_l,np.sqrt(np.mean(snr_array)))
        det_prob[j]=np.sum(ncx2.sf(12**2,df=2*len(ifos_list),nc=snr_array))/N_inj
        j+=1
        
    return det_prob, d_array

            
    
    


# In[5]:


priors_bns=bilby.gw.prior.BNSPriorDict()
priors_bns['mass_1']=bilby.prior.Uniform(minimum=0.5,maximum=3)
priors_bns['mass_2']=bilby.prior.Uniform(minimum=0.5,maximum=3)
priors_bns['mass_ratio']=bilby.prior.Constraint(minimum=0.125, maximum=1)
priors_bns['chirp_mass']=bilby.prior.Constraint(minimum=1.184,maximum=1.4)
priors_bns['chi_1'].maximum=0.89
priors_bns['chi_2'].maximum=0.89





# In[6]:


import h5py

ifos_list = ['H1','L1','V1']

psd_np = np.genfromtxt('GW170817_PSDs.dat')

psds_dict = {}
i=0
for ifo in ifos_list:
    psds_dict[ifo]=bilby.gw.detector.PowerSpectralDensity(frequency_array=psd_np[:,0],
                                      psd_array=psd_np[:,i])
    i+=1


# In[7]:


det_prob, d_array=calculate_det_prob_matrix(1000,duration=64,t_inj=0,sampling_frequency=2048,ifos_list=['H1','L1','V1'],psd_dict=psds_dict,
                         approx='IMRPhenomPv2_NRTidal',d_array=np.linspace(10,200,50),
                         priors_dict=priors_bns)


# In[11]:


data=np.column_stack([d_array,det_prob])
np.savetxt('det_prob_BNS.dat',data)


# In[ ]:




